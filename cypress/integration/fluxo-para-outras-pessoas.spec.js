/// <reference types="cypress" />

import { generate } from 'gerador-validador-cpf';

describe('Fluxo de contratar plano com dependente', () => {
    it('montar plano', () => {
        // home
        cy.visit('http://localhost:4200');
        cy.get('.btn-primary').first().click();

        // dados-pessoais
        cy.get('#dateOfBirth').type('1981-04-26');
        cy.get('#generoSelect > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click({force: true});
        cy.get('mat-option').contains('Masculino').click();
        cy.get('#coberturaSelect > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click({force: true});
        cy.get('mat-option').contains('FORTALEZA').click();
        cy.get('#paraQuemSelect > .mat-select-trigger > .mat-select-value').click({force: true});
        cy.get('mat-option').contains('Para outras pessoas').click();
        cy.get('.mat-card-actions > .mat-focus-indicator > .mat-button-wrapper').click({force: true});
        cy.get('[class="mat-button-wrapper"]').contains('Entendi e concordo').click({force: true});
        
        // contratar-online
        cy.get('#nome').focus().type('Abrão Berthoglio da Silva Goes');
        cy.get('#mat-input-2').focus().type('abraogoes@gmail.com');
        cy.get('#mat-input-3').click();
        cy.get('#mat-input-3').focus().type('(85)99999-9999');
        cy.get('.mat-button-wrapper').click();
        
        // adicionar-dependente
        cy.get('.mat-card-actions > .botao-primary').click();
        
        // dados-beneficiario
        cy.get('#dateOfBirth').type('1983-06-30');
        cy.get('#parentescoSelect').click({force: true});
        cy.get('.mat-datepicker-content').invoke('hide');
        cy.get('.cdk-overlay-container').invoke('hide');
        cy.get('.mat-option-text').contains('IRMAO').click({force: true});
        cy.get('.mat-button-wrapper').contains(" Continuar ").click();

        // adicionar-dependente
        cy.get('[class="mat-button-wrapper"]').contains("Adicione mais beneficiários ").click({force: true});

        // dados-beneficiario
        cy.get('#dateOfBirth').type('1986-04-11');
        cy.get('#parentescoSelect').click({force: true});
        cy.get('.mat-datepicker-content').invoke('hide');
        cy.get('.cdk-overlay-container').invoke('hide');
        cy.get('.mat-option-text').contains('CUNHADO').click({force: true});
        cy.get('.mat-button-wrapper').contains(" Continuar ").click();
        
        // adicionar-dependente
        cy.get('.mat-button-wrapper').contains("Veja os planos pra você ").click();
        
        // prateleira
        cy.get(':nth-child(2) > .mat-card').click();

        // prateleira-detalhe
        cy.get('.div-card > .botao-contrate-plano-online').click();

        // termo-contratacao
        cy.get('.botao-primary').click();

        // cadastro
        cy.get('[fxlayout="column"] > .botao-primary').click();
        
        // compra-stepper/0
        cy.get('.button-big-marked').contains('Dados').first().click();

        // cadastro-dados-pessoais/titular
        cy.get('#cpf').type(`'${generate()}'`);
        cy.get('.cdk-overlay-container').invoke('show');
        cy.get('#estadoCivil > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click({force: true});
        cy.get('mat-option').contains('CASADO').click();
        cy.get('#nomeCompletoMae').type('Liduina Berthoglio Silva');
        cy.get('#tipoDocumento > .mat-select-trigger').click({force: true});
        cy.get('mat-option').contains('Registro Geral com CPF').click();
        cy.get('#numeroDocumento').type('361193233');
        cy.get('.mat-card-actions > .mat-focus-indicator').click();

        // compra-stepper/0
        cy.get('.button-big-marked').contains('Endereço').first().click();

        // cadastro-dados-endereco/titular
        cy.get('.can-toggle__switch').click({force: true});
        cy.get('#cep').type('60180433');
        cy.get('#numero').click({force: true});
        cy.get('#numero').focus().type('888');
        cy.get('#complemento').click({force: true});
        cy.get('#declaro1').click({force: true});
        cy.get('#declaro2').click({force: true});
        cy.get('.mat-card-actions > .mat-focus-indicator').click({force: true});
        cy.get('[class="cdk-overlay-container"]').invoke('hide');
        
        // compra-stepper/0
        cy.get('[style="margin-bottom: 3px; margin-top: 5%; border-radius: 9px"] > [cols="12"] > :nth-child(1) > [colspan="4"] > :nth-child(1) > .internalMatGrid > .mat-grid-list > div > [style="left: 0px; width: calc(((100% - 0px) * 1) + 0px); top: 0px; height: calc(47px);"] > .mat-figure > .button-big-marked').contains('Dados').first().click();
        
        // cadastro-dados-pessoais -> beneficiario 1
        cy.get('#cpf').click({force: true});
        cy.get('#cpf').type(`'${generate()}'`);
        cy.get('.cdk-overlay-container').invoke('show');
        cy.get('#generoSelect > .mat-select-trigger > .mat-select-arrow-wrapper').click({force: true});
        cy.get('#mat-option-250 > .mat-option-text').click({force: true});
        cy.get('.cdk-overlay-container').invoke('hide');
        cy.get('#nomeCompleto').click({force: true});
        cy.get('#nomeCompleto').type('Mário Berthoglio da Silva');
        cy.get('#nomeCompletoMae').type('Liduina Berthoglio da Silva');
        cy.get('.cdk-overlay-container').invoke('show');
        cy.get('#estadoCivil > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click({force: true});
        cy.get('#mat-option-258 > .mat-option-text').click();
        cy.get('#tipoDocumento > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click({force: true});
        cy.get('#mat-option-252 > .mat-option-text').click();
        cy.get('#numeroDocumento').click({force: true});
        cy.get('#numeroDocumento').type('423373833');
        cy.get('.cdk-overlay-container').invoke('hide');
        cy.get('.mat-card-actions > .mat-focus-indicator').click({force: true});

        // compra-stepper/0
        cy.get(':nth-child(3) > [cols="12"] > :nth-child(1) > [colspan="4"] > :nth-child(1) > .internalMatGrid > .mat-grid-list > div > [style="left: 0px; width: calc(((100% - 0px) * 1) + 0px); top: 0px; height: calc(47px);"] > .mat-figure > .button-big-marked').first().click();

        // cadastro-dados-pessoais -> beneficiario 1
        cy.get('#cpf').click({force: true});
        cy.get('#cpf').type(`'${generate()}'`);
        cy.get('.cdk-overlay-container').invoke('show');
        cy.get('#generoSelect > .mat-select-trigger > .mat-select-arrow-wrapper').click({force: true});
        cy.get('#mat-option-265 > .mat-option-text').click();
        cy.get('.cdk-overlay-container').invoke('hide');
        cy.get('#nomeCompleto').click({force: true});
        cy.get('#nomeCompleto').type('Bruna Berthoglio da Silva');
        cy.get('#nomeCompletoMae').type('Irina de Souza');
        cy.get('.cdk-overlay-container').invoke('show');
        cy.get('#estadoCivil > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click({force: true});
        cy.get('#mat-option-272 > .mat-option-text').click();
        cy.get('#tipoDocumento > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click({force: true});
        cy.get('#mat-option-266 > .mat-option-text').click();
        cy.get('#numeroDocumento').click({force: true});
        cy.get('#numeroDocumento').type('471059444');
        cy.get('.cdk-overlay-container').invoke('hide');
        cy.get('.mat-card-actions > .mat-focus-indicator').click({force: true});
        
        // compra-stepper/0
        cy.get('.mat-button-wrapper').contains(" Anexar documentos ").click({force: true});

        // anexar-documentos
        cy.get('[fxlayout="column"] > .botao-primary').click({force: true});
        
        // compra-stepper/1
        cy.get('[class=button-big-marked]').contains('Identificação').first().click({force: true});

        // foto-documentos/indentificacao/titular
        const photo = 'photo-test.jpg';
        cy.get('#inputFrente').attachFile(photo);

        cy.get('#inputVerso').attachFile(photo);

        cy.get('[class=mat-button-wrapper]').contains('Salvar e fechar').click();

        // foto-documentos/endereco/titular
        cy.get('[class=button-big-marked]').contains('Residencia').click({force: true});

        cy.get('#inputFrente').attachFile(photo);
        cy.get('.mat-card-actions > .mat-focus-indicator').click();
        
        //
        cy.get('[style="background-color: rgb(26, 123, 224);"]').contains('Identificação').first().click({force: true});

        // foto-documentos/identificacao/1
        cy.get('#inputFrente').attachFile(photo);

        cy.get('#inputVerso').attachFile(photo);

        cy.get('.cdk-overlay-container').invoke('show');
        cy.get('.mat-select-arrow-wrapper').click({force: true});
        cy.get('#mat-option-278 > .mat-option-text').click();
        cy.get('.cdk-overlay-container').invoke('hide');

        cy.get('#frenteComplementar').attachFile(photo);
        cy.get('#versoComplementar').attachFile(photo);

        cy.get('[class=mat-button-wrapper]').contains('Salvar e fechar').click();

        //
        cy.get('[style="background-color: rgb(26, 123, 224);"]').contains('Identificação').last().click({force: true});

        // foto-documentos/identificacao/2
        cy.get('#inputFrente').attachFile(photo);

        cy.get('#inputVerso').attachFile(photo);

        cy.get('.cdk-overlay-container').invoke('show');
        cy.get('.mat-select-arrow-wrapper').click({force: true});
        cy.get('#mat-option-284 > .mat-option-text').click();
        cy.get('.cdk-overlay-container').invoke('hide');

        cy.get('#frenteComplementar').attachFile(photo);
        cy.get('#versoComplementar').attachFile(photo);

        cy.get('[class=mat-button-wrapper]').contains('Salvar e fechar').click();
        
        // compra-stepper/1
        cy.get('.mat-raised-button').click();

        // declaracao-saude
        cy.get('[fxlayout="column"] > .botao-primary').click({force: true});

        // carta-orientacao
        cy.get('input').click({force: true});
        cy.get('.mat-card-actions > .mat-focus-indicator').click({force: true});

        // compra-stepper/2
        cy.get('#peso').click({force: true});
        cy.get('#peso').focus().type('90');
        cy.get('#altura').click({force: true});
        cy.get('#altura').focus().type('185');

        cy.get('#peso0').click({force: true});
        cy.get('#peso0').focus().type('95');
        cy.get('#altura0').click({force: true});
        cy.get('#altura0').focus().type('190');

        cy.get('#peso1').click({force: true});
        cy.get('#peso1').focus().type('58');
        cy.get('#altura1').click({force: true});
        cy.get('#altura1').focus().type('160');

        cy.get('#cdk-step-content-10-2 > .mat-stroked-button').click({force: true});

        // finalizar-orcamento
        cy.get('.botao-primary').click({force: true});

        cy.get('mat-dialog-container').invoke('show').get('[class=mat-button-wrapper]').contains('Confirmo').click({force: true});
        
    });
});