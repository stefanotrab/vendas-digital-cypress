/// <reference types="cypress" />

import { generate } from 'gerador-validador-cpf';

describe('Fluxo de contratar plano', () => {
    it('montar plano', () => {
        // home
        cy.visit('http://app-homolog.hapvida.com.br/contrateonline');
        // cy.visit('https://www.hapvida.com.br/contrateonline');
        // cy.visit('http://localhost:4200');
        cy.get('.btn-primary').first().click();

        // dados-pessoais
        cy.get('#dateOfBirth').type('1989-03-21');
        cy.get('#generoSelect > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click({force: true});
        cy.get('mat-option').contains('Masculino').click();
        cy.get('#coberturaSelect > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click({force: true});
        cy.get('mat-option').contains('FORTALEZA').click();
        cy.get('#paraQuemSelect > .mat-select-trigger > .mat-select-value').click({force: true});
        cy.get('mat-option').contains('Só para mim').click();
        cy.get('.mat-card-actions > .mat-focus-indicator > .mat-button-wrapper').click({force: true});

        // contratar-online
        cy.get('#nome').focus().type('Pedro Oliveira Paes');
        cy.get('#mat-input-2').focus().type('pedro.oliveira@gmail.com');
        cy.get('#mat-input-3').click();
        cy.get('#mat-input-3').focus().type('(85)99999-9999');
        cy.get('.mat-button-wrapper').click();

        // prateleira
        cy.get(':nth-child(1) > .mat-card').click();

        // prateleira-detalhe
        cy.get('.div-card > .botao-contrate-plano-online').click();

        // termo-contratação
        cy.get('.botao-primary').click();

        // cadastro
        cy.get('[fxlayout="column"] > .botao-primary').click();

        // compra-stepper/0
        cy.get('[class=button-big-marked]').contains('Dados').click();

        // cadastro-dados-pessoais/titular
        cy.get('#cpf').type(`'${generate()}'`);
        cy.get('#estadoCivil > .mat-select-trigger > .mat-select-value > .mat-select-placeholder').click({force: true});
        cy.get('mat-option').contains('SOLTEIRO').click();
        cy.get('#nomeCompletoMae').type('Maria Oliveira Paes');
        cy.get('#tipoDocumento > .mat-select-trigger').click({force: true});
        cy.get('mat-option').contains('Registro Geral com CPF').click();
        cy.get('#numeroDocumento').type('232149641');
        cy.get('.mat-card-actions > .mat-focus-indicator').click();

        // compra-stepper/0
        cy.get('[class=button-big-marked]').contains('Endereço').click();

        // cadastro-dados-endereco/titular
        cy.get('.can-toggle__switch').click({force: true});
        cy.get('#cep').type('60192275');
        cy.get('#numero').click({force: true});
        cy.get('#numero').focus().type('777');
        cy.get('#complemento').click({force: true});
        cy.get('#complemento').type('Igreja Católica');

        cy.get('#declaro1').click({force: true});
        cy.get('#declaro2').click({force: true});
        cy.get('.mat-card-actions > .mat-focus-indicator').click({force: true});
        cy.get('[class="cdk-overlay-container"]').invoke('hide');

        // compra-stepper/0
        cy.get('#cdk-step-content-2-0 > .mat-stroked-button').click({force: true});

        // anexar-documentos
        cy.get('[fxlayout="column"] > .botao-primary').click({force: true});

        // compra-stepper/1
        cy.get('[class=button-big-marked]').contains('Identificação').click({force: true});

        // foto-documentos/indentificacao/titular
        const photo = 'photo-test.jpg';
        cy.get('#inputFrente').attachFile(photo);

        cy.get('#inputVerso').attachFile(photo);

        cy.get('[class=mat-button-wrapper]').contains('Salvar e fechar').click();
        
        // foto-documentos/endereco/titular
        cy.get('[class=button-big-marked]').contains('Residencia').click({force: true});

        cy.get('#inputFrente').attachFile(photo);
        cy.get('.mat-card-actions > .mat-focus-indicator').click();

        cy.get('.mat-raised-button').click();

        // declaracao-saude
        cy.get('[fxlayout="column"] > .botao-primary').click({force: true});

        // carta-orientacao
        cy.get('input').click({force: true});
        cy.get('.mat-card-actions > .mat-focus-indicator').click({force: true});

        // compra-stepper/2
        cy.get('#peso').click({force: true});
        cy.get('#peso').focus().type('81');
        cy.get('#altura').click({force: true});
        cy.get('#altura').focus().type('178');
        cy.get('#cdk-step-content-6-2 > .mat-stroked-button').click({force: true});
        
        // finalizar-orcamento
        cy.get('.botao-primary').click({force: true});

        cy.get('mat-dialog-container').invoke('show').get('[class=mat-button-wrapper]').contains('Confirmo').click({force: true});
    });
});